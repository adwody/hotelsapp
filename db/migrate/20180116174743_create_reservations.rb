class CreateReservations < ActiveRecord::Migration[5.1]
  def change
    create_table :reservations do |t|
      t.integer :user_id
      t.integer :hotel_id
      t.integer :room_id
      t.date :from
      t.date :to

      t.timestamps
    end
  end
end
