class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.integer :permissions
      t.integer :hotel_id
      t.string :login
      t.string :password

      t.timestamps
    end
  end
end
