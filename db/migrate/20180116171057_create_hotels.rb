class CreateHotels < ActiveRecord::Migration[5.1]
  def change
    create_table :hotels do |t|
      t.string :name
      t.string :address
      t.string :postcode
      t.string :city

      t.timestamps
    end
  end
end
