class CreateRooms < ActiveRecord::Migration[5.1]
  def change
    create_table :rooms do |t|
      t.integer :hotel_id
      t.integer :size
      t.decimal :price, precision: 7, scale: 2

      t.timestamps
    end
  end
end
