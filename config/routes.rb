Rails.application.routes.draw do

  root to: 'website#index'
  get 'show/:id/:from/:to', to: 'website#show'
  get 'new_reservation/:id/:room_id/:from/:to', to: 'website#new_reservation'
  get 'signin', to: 'website#signin'
  post 'signin', to: 'website#signin'
  get 'logout', to: 'website#logout'
  get 'admin', to: 'website#admin', as: 'admin'
  get 'admin/hotel/', to: 'website#hotel_admin', as: 'hotel_admin'
  resources :reservations
  resources :rooms
  resources :users
  resources :hotels
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
