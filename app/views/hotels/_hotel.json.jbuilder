json.extract! hotel, :id, :name, :address, :postcode, :city, :created_at, :updated_at
json.url hotel_url(hotel, format: :json)
