json.extract! reservation, :id, :user_id, :hotel_id, :room_id, :from, :to, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)
