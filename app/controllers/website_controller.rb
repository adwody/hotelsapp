class WebsiteController < ApplicationController
  def index
    @hotels = Hotel.where(city: params[:city])
    @rooms = Room.all
    @reservations = Reservation.all
    @from = params[:from]
    @to = params[:to]
    @today = Date.today
    @tomorrow = Date.tomorrow
  end

  def show
    @id = params[:id]
    @from = Date.parse(params[:from])
    @to = Date.parse(params[:to])
    @hotel = Hotel.find(@id)
    @rooms = Room.where(hotel_id: @id)

    / @roomTmp =  Room.where(hotel_id: @id).joins(:reservation).where(["date(reservations.from) > ?", @from]) /
  
  end

  def new_reservation 
    @id = params[:id]
    @room = params[:room_id]
    @from = params[:from]
    @to = params[:to]
    @reservation = Reservation.create(user_id: 1, hotel_id: @id, room_id: @room, from: @from, to: @to)

  end

  def signin
    if request.post?
      @login = params[:login]
      @password = params[:password]
      user = User.where(login: @login, password:@password).first
      if user
        session[:hotel_id] =  user.hotel_id
        session[:permissions] =  user.permissions
        if user.permissions > 0
          redirect_to hotel_admin_url
        else
          redirect_to admin_url
        end
      else
        @info = "No user with this data. Try again!"
      end
    end
  end

  def logout
    session[:hotel_id] =  nil
    session[:permissions] = nil
    redirect_to signin_url
  end

  def admin
    if session[:permissions] != 0
      redirect_to signin_url
    end
  end

  def hotel_admin
    if session[:permissions] == 0
      redirect_to signin_url
    end
    @reservations = Reservation.where(hotel_id: session[:hotel_id])
    @rooms = Room.all
  end

end
